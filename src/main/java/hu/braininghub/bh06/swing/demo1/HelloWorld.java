package hu.braininghub.bh06.swing.demo1;

import javax.swing.JButton;
import javax.swing.JFrame;

public class HelloWorld {

	
	public static void main(String[]args) {
		
		JFrame frame = new JFrame("Hello World");
		
		frame.setSize(200, 200);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(null);
		JButton jButtonPressMe = new JButton("");
		jButtonPressMe.addActionListener(new UIController());
		jButtonPressMe.setSize(200,100);
		jButtonPressMe.setLocation(30, 30);
		jButtonPressMe.setToolTipText("Ez egy hasznos gomb...");
		jButtonPressMe.setText("Nyomj meg!");
		jButtonPressMe.setName("jButtonPressMe");
		frame.add(jButtonPressMe);
		frame.setVisible(true);
	}
}
