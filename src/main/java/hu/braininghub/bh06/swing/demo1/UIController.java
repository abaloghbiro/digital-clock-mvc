package hu.braininghub.bh06.swing.demo1;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

import javax.swing.JOptionPane;

public class UIController implements ActionListener {

	public void actionPerformed(ActionEvent e) {
		System.out.println(e.getSource());
		Toolkit.getDefaultToolkit().beep();
		JOptionPane.showMessageDialog(null, "Button pressed: "+new Date(e.getWhen()));
	}

}
