package hu.braininghub.bh06.swing.registration;

import java.util.ArrayList;
import java.util.List;

public class RegistrationController {

	private Model model;

	public RegistrationController() {
		model = new Model();
	}

	public Model getModel() {
		return model;
	}

	public void registration(String firstname, String lastname) {

		List<String> errors = new ArrayList<>();

		if (firstname == null || firstname.isEmpty()) {
			errors.add("Firstname cannot be null or empty!");
		}
		if (lastname == null || lastname.isEmpty()) {
			errors.add("Lastname cannot be null or empty!");
		}

		model.addErrorMessages(errors);

		if (!model.hasValidationErrors()) {
			UserDTO dto = new UserDTO();
			dto.setFirstname(firstname);
			dto.setLastname(lastname);
			model.setDto(dto);
		}

	}

}
