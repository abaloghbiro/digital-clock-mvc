package hu.braininghub.bh06.swing.registration;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Model {

	private UserDTO dto;

	private List<View> subscribers = new ArrayList<View>();

	private List<String> errorMessages = new ArrayList<>();

	public void subscribe(View v) {
		subscribers.add(v);
	}

	public void unSubscribe(View v) {
		subscribers.remove(v);
	}

	public UserDTO getDto() {
		return dto;
	}

	public void setDto(UserDTO dto) {
		this.dto = dto;
		subscribers.forEach(v -> v.refreshView());
	}

	public void addErrorMessages(List<String> errors) {
		errorMessages = errors;
		if (errorMessages.size() > 0) {
			subscribers.forEach(v -> v.refreshView());
		}
	}

	public List<String> getErrorMessages() {
		return Collections.unmodifiableList(errorMessages);
	}

	public boolean hasValidationErrors() {
		return !errorMessages.isEmpty();
	}

}
