package hu.braininghub.bh06.swing.registration;

public interface View {

	void refreshView();
	
	void initView();
}
