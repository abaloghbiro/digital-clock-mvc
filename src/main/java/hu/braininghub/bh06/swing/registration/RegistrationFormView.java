package hu.braininghub.bh06.swing.registration;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class RegistrationFormView implements View, ActionListener {

	private RegistrationController controller;
	private JLabel informationLabel;
	private JTextField firstnameTextField;
	private JTextField lastnameTextField;
	private int rowNumber = 4;
	private int colNumber = 2;
	private JPanel[][] panelHolder = new JPanel[rowNumber][colNumber];

	public RegistrationFormView(RegistrationController controller) {
		this.controller = controller;
		initView();
		this.controller.getModel().subscribe(this);
	}

	@Override
	public void initView() {
		JFrame frame = new JFrame("Registration form");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(new GridLayout(rowNumber, colNumber));

		for (int i = 0; i < rowNumber; i++) {

			for (int j = 0; j < colNumber; j++) {
				panelHolder[i][j] = new JPanel();
				panelHolder[i][j].setLayout(new GridLayout(1, 1));
				frame.add(panelHolder[i][j]);
			}
		}

		informationLabel = new JLabel();
		informationLabel.setFont(new Font("Arial", Font.BOLD, 40));
		informationLabel.setForeground(Color.RED);
		panelHolder[0][1].add(informationLabel);
		JLabel firstnameLabel = new JLabel("First name");
		panelHolder[1][0].add(firstnameLabel);
		firstnameTextField = new JTextField();
		firstnameTextField.setFont(new Font("Arial", Font.BOLD, 40));
		firstnameLabel.setFont(new Font("Arial", Font.BOLD, 40));
		firstnameTextField.setSize(200, 100);
		panelHolder[1][1].add(firstnameTextField);
		JLabel lastnameLabel = new JLabel("Last name");
		lastnameLabel.setFont(new Font("Arial", Font.BOLD, 40));
		panelHolder[2][0].add(lastnameLabel);
		lastnameTextField = new JTextField();
		lastnameTextField.setFont(new Font("Arial", Font.BOLD, 40));
		firstnameTextField.setSize(200, 100);
		panelHolder[2][1].add(lastnameTextField);
		JButton registrationButton = new JButton("Registration");
		registrationButton.addActionListener(this);
		panelHolder[3][1].add(registrationButton);

		frame.pack();
		frame.setVisible(true);

	}

	@Override
	public void refreshView() {

		if (this.controller.getModel().hasValidationErrors()) {
			informationLabel.setForeground(Color.RED);
			informationLabel.setText(String.join(",", controller.getModel().getErrorMessages()));
		} else {
			UserDTO dto = controller.getModel().getDto();
			if (dto != null) {
				firstnameTextField.setText(dto.getFirstname());
				lastnameTextField.setText(dto.getLastname());
			}
			informationLabel.setForeground(Color.GREEN);
			informationLabel.setText("Your registration was successfull!");
		}

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		controller.registration(firstnameTextField.getText(), lastnameTextField.getText());
	}

}
