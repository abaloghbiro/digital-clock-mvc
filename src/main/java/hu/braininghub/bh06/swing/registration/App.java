package hu.braininghub.bh06.swing.registration;

public class App {

	public static void main(String[]args) {
		
		RegistrationController c = new RegistrationController();
		
		new RegistrationFormView(c);
	}
}
