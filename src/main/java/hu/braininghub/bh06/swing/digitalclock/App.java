package hu.braininghub.bh06.swing.digitalclock;

public class App {

	public static void main(String[] args) {

		
		// Automatic update trigger
		DigitalClockModel modelForAutomaticCase = new DigitalClockModel();
		DigitalClockController controllerForAutomaticCase = new DigitalClockController(modelForAutomaticCase);
		new TimerBasedSwingDigitalClockView(controllerForAutomaticCase);
		new ConsoleDigitalClockView(controllerForAutomaticCase);
		
		
		// Manual update trigger
		DigitalClockModel modelForManualCase = new DigitalClockModel();
		DigitalClockController controllerForManualCase = new DigitalClockController(modelForManualCase);
		new UserBasedSwingDigitalClockView(controllerForManualCase);
	}

}
