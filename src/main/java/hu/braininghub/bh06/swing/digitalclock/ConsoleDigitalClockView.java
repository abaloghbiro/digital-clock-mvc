package hu.braininghub.bh06.swing.digitalclock;

public class ConsoleDigitalClockView extends AbstractDigitalClockView {

	public ConsoleDigitalClockView(DigitalClockController controller) {
		super(controller);
	}

	public void refreshView() {
		System.out.println(controller.getModel().getValue());
	}

	@Override
	public void initView() {
		// NOT IMPLEMENTED YET
	}

}
