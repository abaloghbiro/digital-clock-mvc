package hu.braininghub.bh06.swing.digitalclock;

import java.util.ArrayList;
import java.util.List;

public class DigitalClockModel {

	private String value;

	private List<DigitalClockView> subscribers = new ArrayList<DigitalClockView>();

	public void subscribe(DigitalClockView v) {
		subscribers.add(v);
	}

	public void unSubscribe(DigitalClockView v) {
		subscribers.remove(v);
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
		subscribers.forEach(v -> v.refreshView());
	}

}
