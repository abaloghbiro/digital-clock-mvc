package hu.braininghub.bh06.swing.digitalclock;

import java.text.DateFormat;
import java.util.Date;

public class DigitalClockController {

	private DigitalClockModel model;

	public DigitalClockController(DigitalClockModel m) {
		this.model = m;
	}

	public DigitalClockModel getModel() {
		return model;
	}

	public void handleRequest() {
		this.model.setValue(DateFormat.getTimeInstance(DateFormat.MEDIUM).format(new Date()));
	}

}
