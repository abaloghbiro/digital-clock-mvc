package hu.braininghub.bh06.swing.digitalclock;

import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

public class UserBasedSwingDigitalClockView extends AbstractDigitalClockView implements ActionListener {

	private JLabel dateLabel;

	public UserBasedSwingDigitalClockView(DigitalClockController controller) {
		super(controller);
	}

	public void refreshView() {
		dateLabel.setText(controller.getModel().getValue());
	}

	@Override
	public void initView() {

		JFrame frame = new JFrame("Digital clock");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(200, 100);
		frame.setResizable(false);
		dateLabel = new JLabel();
		dateLabel.setHorizontalAlignment(SwingConstants.CENTER);
		dateLabel.setFont(new Font("Arial", Font.BOLD, 40));
		frame.add(dateLabel);
		frame.setLayout(new FlowLayout());
		JButton refreshTime = new JButton("Refresh clock!");
		refreshTime.addActionListener(this);
		JButton refreshTime2 = new JButton("Refresh clock2!");
		refreshTime2.setActionCommand("refresh2");
		refreshTime2.addActionListener(this);
		frame.add(refreshTime);
		frame.add(refreshTime2);
		frame.setVisible(true);
	}

	public void actionPerformed(ActionEvent e) {
			this.controller.handleRequest();		
	}

}
