package hu.braininghub.bh06.swing.digitalclock;

public interface DigitalClockView {

	void refreshView();
	
	void initView();
}
