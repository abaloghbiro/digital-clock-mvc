package hu.braininghub.bh06.swing.digitalclock;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.Timer;

public class TimerBasedSwingDigitalClockView extends AbstractDigitalClockView implements ActionListener {

	private JLabel dateLabel;
	private Timer timer = new Timer(0, this);

	public TimerBasedSwingDigitalClockView(DigitalClockController controller) {
		super(controller);
		timer.start();
		timer.setDelay(1000);
	}

	public void refreshView() {
		dateLabel.setText(controller.getModel().getValue());
	}

	@Override
	public void initView() {

		JFrame frame = new JFrame("Digital clock");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(200, 100);
		frame.setResizable(false);
		dateLabel = new JLabel();
		dateLabel.setHorizontalAlignment(SwingConstants.CENTER);
		dateLabel.setFont(new Font("Arial", Font.BOLD, 40));
		frame.add(dateLabel);
		frame.setVisible(true);
	}

	public void actionPerformed(ActionEvent e) {
		this.controller.handleRequest();
	}

}
