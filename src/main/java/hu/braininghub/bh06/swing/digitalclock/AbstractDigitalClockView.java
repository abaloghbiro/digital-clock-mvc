package hu.braininghub.bh06.swing.digitalclock;

public abstract class AbstractDigitalClockView implements DigitalClockView {

	protected DigitalClockController controller;

	public AbstractDigitalClockView(DigitalClockController controller) {
		initView();
		this.controller = controller;
		this.controller.getModel().subscribe(this);
	}
}
