package hu.braininghub.bh06.swing.demo2;

import java.awt.Dimension;
import java.awt.Toolkit;

public class DisplayAttributes {

	
	public static void main(String[]args) {
		
		Toolkit tk = Toolkit.getDefaultToolkit();
		Dimension dim = tk.getScreenSize();
		
		System.out.println("Diplay size: "+dim.getWidth() + "/ "+dim.getHeight()+" px");
		System.out.print("Resolution: "+tk.getScreenResolution());
	}
}
